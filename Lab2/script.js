window.onload = function(){
    
    document.getElementById("page-navigation-menu-dashboard").click()

    let tableBody = document.querySelector("#students-table tbody");

    for(let i = 0; i < students.length; i++)
    {
        students[i].status = Math.floor(Math.random()*2);
    }

    let cntr = 0;
    students.forEach( std=>{
        let status = (students[cntr].status===1)?"students-table-status-green":"students-table-status-grey";

        var newRow = document.createElement("tr");
        newRow.id = std.studentId;
        newRow.innerHTML = `
        <td><input type="checkbox" class="students-table-checkbox"></td>
        <td>${std.group}</td>
        <td>${std.firstName} ${std.lastName}</td>
        <td>${std.gender}</td>
        <td>${std.birthday}</td>
        <td><div class=`+status+`></div></td>
        <td>
            <div class="students-table-options-cell-container">
                <div class="students-table-options-edit">
                    <button class="btn btn-outline-success students-table-options-edit-button">
                        <img src="./res/pencil.png" alt="Table edit button">
                    </button>
                </div>
                <div class="students-table-options-delete">
                    <button class="btn btn-outline-danger students-table-options-delete-button">
                        <img src="./res/close.png" alt="Table delete button">
                    </button>
                </div>
            </div>
        </td>
        `;

        tableBody.appendChild(newRow);
        cntr++;
    });
    
    StudentButtonsInit(); 
}

let id = 0
class Student {
    constructor(group, firstName, lastName, gender, birthday,status) {
      this.group = group;
      this.firstName = firstName;
      this.lastName = lastName;
      this.gender = gender;
      this.birthday = birthday;
      this.status = status;

      this.studentId = id;
      id++
    }
}


document.getElementById('notification-icon').addEventListener('click', function() {
    let dropdown = document.getElementById('inbox-dropdown');
    if (dropdown.style.display === 'block') {
        dropdown.style.display = 'none';
    } else {
        dropdown.style.display = 'block';
        document.getElementById('profile-dropdown').style.display = 'none';
    }
});

document.getElementById('page-header-right-user-summary').addEventListener('click', function() {
    let dropdown = document.getElementById('profile-dropdown');
    if (dropdown.style.display === 'block') {
        dropdown.style.display = 'none';
    } else {
        dropdown.style.display = 'block';
        document.getElementById('inbox-dropdown').style.display = 'none';
    }
});

document.getElementById('students-content-button-add').addEventListener('click', function(e) {
    switchToEdit(false);

    let form = document.getElementById('add-student-form');
    if (form.style.display === 'block') {
        form.style.display = 'none';
        document.getElementById('overlay').style.display = 'none';
    } else {
        form.style.display = 'block';
        document.getElementById('overlay').style.display = 'block';
    }
});


function showContent(contentId){
    let allContent = document.querySelectorAll("#page-working-section-right > div");
    allContent.forEach(function(content){
        content.style.display = "none";
    });

    let selectedContent = document.getElementById(contentId + '-content');
    if(selectedContent){
        selectedContent.style.display = 'block';
    }
}

function showConfirmation(studentName) {
    let confirmationBody = document.querySelector(".confirmation-body");
    confirmationBody.textContent = "Are you sure you want to delete " + studentName + "?";
    document.getElementById('confirmation').style.display = 'block';
}

function hideConfirmation() {
    document.getElementById('confirmation').style.display = 'none';
}

function hideAddStudentForm(){
    document.getElementById('add-student-form').style.display = 'none';
    document.getElementById('overlay').style.display = 'none';
}

function showStudents()
{
    students.forEach(st=>{
        console.log(st)
    })
}

function showAlert(text = "An error has occurred") {
    const myAlert = $("#alert-danger");
    myAlert.find(".alert-text").text(text);

    myAlert.css("opacity", "0");
    myAlert.css("visibility", "unset");

    myAlert.animate({ opacity: 1 }, 500);

    setTimeout(function() {
        myAlert.animate({ opacity: 0 }, 1000, function() {
            myAlert.css("visibility", "hidden");
        });
    }, 5000);
}
