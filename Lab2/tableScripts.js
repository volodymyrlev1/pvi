let students = [
    new Student("PZ-22", "Volodymyr", "Lev", "M", "2005-07-30"),
    new Student("PZ-22", "Artur", "Husar", "M", "2005-01-11"),
    new Student("PZ-22", "Oleh", "Shmyheliuk", "M", "2005-05-29")
];

function addStudent(){
    var group = document.querySelector("#group").value;
    var firstName = document.querySelector("#first-name").value;
    var lastName = document.querySelector("#last-name").value;
    var gender = document.querySelector("#gender").value;
    var birthday = document.querySelector("#birthday").value;
    var status = 0;

    var nameRegex = /^[a-zA-Zа-яА-Я ]+$/;

    if(firstName.length === 0){
        showAlert("First name field is empty!");
        return;
    }

    if(lastName.length === 0){
        showAlert("Last name field is empty!");
        return;
    }

    if (!nameRegex.test(firstName)) {
        showAlert("First name should consist only of letters!")
        return;
    }
    if (!nameRegex.test(lastName)) {
        showAlert("Last name should consist only of letters!")
        return;
    }

    let std = new Student(group, firstName, lastName, gender,birthday, status);

    students.push(std);

    var tableBody = document.querySelector("#students-table tbody");
    var newRow = document.createElement("tr");
    newRow.id = std.studentId;

    console.log(group,firstName,lastName,gender,birthday);

    var statusSet = (status===1)?"students-table-status-green":"students-table-status-grey";

    newRow.innerHTML = `
        <td><input type="checkbox" class="students-table-checkbox"></td>
        <td>${group}</td>
        <td>${firstName} ${lastName}</td>
        <td>${gender}</td>
        <td>${birthday}</td>
        <td><div class="${statusSet}"></div></td>
        <td>
            <div class="students-table-options-cell-container">
                <div class="students-table-options-edit">
                    <button class="btn btn-outline-success students-table-options-edit-button">
                        <img src="./res/pencil.png" alt="Table edit button">
                    </button>
                </div>
                <div class="students-table-options-delete">
                    <button class="btn btn-outline-danger students-table-options-delete-button">
                        <img src="./res/close.png" alt="Table delete button">
                    </button>
                </div>
            </div>
        </td>
    `;

    tableBody.appendChild(newRow);

    sendDataToServer();

    document.getElementById("group").value = "";
    document.getElementById("first-name").value = "";
    document.getElementById("last-name").value = "";
    document.getElementById("gender").value = "";
    document.getElementById("birthday").value = "";
    
    hideAddStudentForm();
    
    StudentButtonsInit();
}


function editStudent(row, student){
    student.group = $("#group").val();
    student.firstName = $("#first-name").val();
    student.lastName = $("#last-name").val();
    student.gender = $("#gender").val();
    student.birthday = $("#birthday").val();

    var nameRegex = /^[a-zA-Zа-яА-Я ]+$/;

    if(student.firstName.length === 0){
        showAlert("First name field is empty!");
        return;
    }

    if(student.lastName.length === 0){
        showAlert("Last name field is empty!");
        return;
    }

    if (!nameRegex.test(student.firstName)) {
        showAlert("First name should consist only of letters!")
        return;
    }
    if (!nameRegex.test(student.lastName)) {
        showAlert("Last name should consist only of letters!")
        return;
    }

    row.find('td:nth-child(2)').text(student.group);
    row.find('td:nth-child(3)').text(student.firstName + " " + student.lastName);
    row.find('td:nth-child(4)').text(student.gender);
    row.find('td:nth-child(5)').text(student.birthday);

    sendDataToServer();

    hideAddStudentForm();
}

function switchToEdit(_switch) {
    const header =  form.querySelector("h2");
    const btn = form.querySelector("#buttonAddEdit");

    const row = $('#students-table #'+currentRowId);

    if(_switch) {

        let group = row.find('td:nth-child(2)').text(); 
        let name = row.find('td:nth-child(3)').text(); 
        let firstName = name.split(' ')[0];
        let lastName = name.split(' ')[1];
        let gender = row.find('td:nth-child(4)').text(); 
        let birthday = row.find('td:nth-child(5)').text();

        let student;

        students.forEach((st,index)=>{
            if(st.group == group && st.firstName == firstName && st.lastName == lastName)
            {
                student = students[index];
            }
        });

        document.getElementById("group").value = group;
        document.getElementById("first-name").value = firstName;
        document.getElementById("last-name").value = lastName;
        document.getElementById("gender").value = gender;
        document.getElementById("birthday").value = birthday;
        console.log("EDIT MODE");
        header.innerHTML = "Edit Student";
        btn.innerHTML = "Save";

        const newButton = btn.cloneNode(true);
        btn.parentNode.replaceChild(newButton,btn);
        
        newButton.addEventListener('click', function(){
            editStudent(row, student)
        });

    } else {
        document.getElementById("group").value = "";
        document.getElementById("first-name").value = "";
        document.getElementById("last-name").value = "";
        document.getElementById("gender").value = "";
        document.getElementById("birthday").value = "";

        console.log("ADD MODE");
        header.innerHTML = "Add Student";
        btn.innerHTML = "Create";

        const newButton = btn.cloneNode(true);
        btn.parentNode.replaceChild(newButton,btn);

        newButton.addEventListener('click', function(){
            addStudent();
        });
    }
}

let currentRowId;
let rowToDelete;
let form = document.querySelector('#add-student-form');

function StudentButtonsInit() {
    let deleteButtons = document.querySelectorAll(".students-table-options-delete-button");
    let editButtons = document.querySelectorAll(".students-table-options-edit-button");

    
    editButtons.forEach(btn =>{

        console.log("EDITS INITED");

        btn.addEventListener('click',function(e)
        {
            currentRowId = e.target.closest('tr').id;
            switchToEdit(true);
            form.style.display = 'block';
            document.getElementById('overlay').style.display = 'block';
        });
    });

    // delete buttons
    for(let i = 0; i < deleteButtons.length; i++)
    {
        deleteButtons[i].addEventListener("click", function(){
            
            rowToDelete = this.closest("tr");
            let studentName = rowToDelete.querySelector("td:nth-child(3)").textContent;
            showConfirmation(studentName);

            document.getElementById("confirmation-button-ok").addEventListener("click", function() {
                deleteRow(rowToDelete);
                hideConfirmation();
            });

            // Cancel button
            document.getElementById("confirmation-button-cancel").addEventListener("click", function() {
                hideConfirmation();
            });
        });
    }

    // checkboxes
    let headerCheckbox = document.getElementById('students-table-header-checkbox');
    let rowCheckboxes = document.querySelectorAll('.students-table-checkbox');

    headerCheckbox.addEventListener('click', function() {
        rowCheckboxes.forEach(checkbox => {
          checkbox.checked = headerCheckbox.checked;
        });
      });
      
      rowCheckboxes.forEach(checkbox => {
          checkbox.addEventListener('click', () => {
            const allChecked = Array.from(rowCheckboxes).every(checkbox => checkbox.checked);
            headerCheckbox.checked = allChecked;
          });
      });
}

function deleteRow(row) {
    let name = row.querySelector("td:nth-child(3)").textContent;
    let group = row.querySelector('td:nth-child(2)').textContent;
    let nameParts = name.split(' ');
    let firstName = nameParts[0];
    let lastName = nameParts[1];

    students.forEach((st,index)=>{
        if(st.group == group && st.firstName == firstName && st.lastName == lastName)
        {
            students.splice(index,1);
        }
    })

    row.remove();
    hideConfirmation();
}