function sendDataToServer() {
    let group = $("#group").val();
    let firstName = $("#first-name").val();
    let lastName = $("#last-name").val();
    let gender = $("#gender").val();
    let birthday = $("#birthday").val();

    var dataToSend = "group=" + encodeURIComponent(group) + "&firstName=" + encodeURIComponent(firstName) + "&lastName=" + encodeURIComponent(lastName) + "&gender=" + encodeURIComponent(gender) + "&birthday=" + encodeURIComponent(birthday);

    $.ajax({
        type: "POST",
        url: "https://jsonplaceholder.typicode.com/posts",
        data: dataToSend,
        success: function(response) {
            console.log(response);
            console.log("Sent.");
        },
        error: function(xhr, status, error) {
            console.error("Failed. Error :", error);
        }
    });
}