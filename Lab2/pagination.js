document.addEventListener("DOMContentLoaded", function() {
    const paginationButtons = document.querySelectorAll('.pagination-button');
    const maxPage = paginationButtons.length - 2; 
  
    paginationButtons.forEach(button => {
      button.addEventListener('click', function(event) {
        event.preventDefault();
        let nextPage;
  
        if (this.getAttribute('data-page') === '-1') {
          nextPage = parseInt(document.querySelector('.pagination-button.active').getAttribute('data-page')) - 1;
        } else if (this.getAttribute('data-page') === '+1') {
          nextPage = parseInt(document.querySelector('.pagination-button.active').getAttribute('data-page')) + 1;
        } else {
          nextPage = parseInt(this.getAttribute('data-page'));
        }
  
        nextPage = Math.max(1, Math.min(maxPage, nextPage));
  
        const nextButton = document.querySelector(`[data-page="${nextPage}"]`);
  
        // Update active class
        paginationButtons.forEach(btn => {
          btn.classList.remove('active');
        });
        nextButton.classList.add('active');
  
        console.log('Next Page:', nextPage);
      });
    });
  });