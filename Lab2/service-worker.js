const CACHE_NAME = 'lab2-cache-v1';
const urlsToCache = [
  '/',
  '/index.html',
  '/style.css',
  '/script.js',
  '/tableScripts.js',
  '/pagination.js',
  '/server.js',
  '/jquery.js',
  '/jquery.min.js',
  '/jsz/jqModal.js',
  '/jsz/cache/03565abe.js',
  '/jsz/adriver.core.2.js'
];

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        if (response) {
          return response;
        }
        return fetch(event.request);
      })
  );
});
