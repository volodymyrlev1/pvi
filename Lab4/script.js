
window.onload = function(){

    fetchData();

    document.getElementById("page-navigation-menu-students").click()

    StudentButtonsInit(); 
}

let id = 0
class Student {
    constructor(group, firstName, lastName, gender, birthday, status) {
      this.group = group;
      this.firstName = firstName;
      this.lastName = lastName;
      this.gender = gender;
      this.birthday = birthday;
      this.status = status;

      const timeStamp = Date.now().toString(36);
      const randomNumber = Math.random().toString(36).split(".")[1];

      this.studentId = timeStamp+randomNumber; 
    }
}


document.getElementById('notification-icon').addEventListener('mouseover', function() {
    let dropdown = document.getElementById('inbox-dropdown');
    if (dropdown.style.display === 'block') {
        dropdown.style.display = 'none';
    } else {
        dropdown.style.display = 'block';
        document.getElementById('profile-dropdown').style.display = 'none';
    }
});

document.getElementById('inbox-dropdown').addEventListener('mouseout', function(event) {
    if (!event.relatedTarget || (event.relatedTarget.id !== 'notification-icon' && event.relatedTarget.id !== 'inbox-dropdown')) {
        let dropdown = document.getElementById('inbox-dropdown');
        dropdown.style.display = 'none';
    }
});

document.getElementById('notification-icon').addEventListener('click', function() {
    showContent('messages');
});

document.getElementById('page-header-right-user-summary').addEventListener('click', function() {
    let dropdown = document.getElementById('profile-dropdown');
    if (dropdown.style.display === 'block') {
        dropdown.style.display = 'none';
    } else {
        dropdown.style.display = 'block';
        document.getElementById('inbox-dropdown').style.display = 'none';
    }
});

document.getElementById('students-content-button-add').addEventListener('click', function(e) {
    switchToEdit(false);

    let form = document.getElementById('add-student-form');
    if (form.style.display === 'block') {
        form.style.display = 'none';
        document.getElementById('overlay').style.display = 'none';
    } else {
        form.style.display = 'block';
        document.getElementById('overlay').style.display = 'block';
    }
});


function showContent(contentId){
    let allContent = document.querySelectorAll("#page-working-section-right > div");
    allContent.forEach(function(content){
        content.style.display = "none";
    });

    let selectedContent = document.getElementById(contentId + '-content');
    if(selectedContent){
        selectedContent.style.display = 'block';
    }
}

function showConfirmation(studentName) {
    let confirmationBody = document.querySelector(".confirmation-body");
    confirmationBody.textContent = "Are you sure you want to delete " + studentName + "?";
    document.getElementById('confirmation').style.display = 'block';
}

function hideConfirmation() {
    document.getElementById('confirmation').style.display = 'none';
}

function hideAddStudentForm(){
    document.getElementById('add-student-form-container').reset();
    document.getElementById('add-student-form').style.display = 'none';
    document.getElementById('overlay').style.display = 'none';
}

function showStudents()
{
    students.forEach(st=>{
        console.log(st)
    })
}

function showAlert(text = "An error has occurred") {
    const myAlert = $("#alert-danger");
    myAlert.find(".alert-text").text(text);

    myAlert.css("opacity", "0");
    myAlert.css("visibility", "unset");

    myAlert.animate({ opacity: 1 }, 500);

    setTimeout(function() {
        myAlert.animate({ opacity: 0 }, 1000, function() {
            myAlert.css("visibility", "hidden");
        });
    }, 5000);
}

function addStudentsToTheTable(student){
    let tableBody = document.querySelector("#students-table tbody");

    let group = student.group;
    let firstName = student.firstName;
    let lastName = student.lastName;
    let gender = student.gender;
    let birthday = student.birthday;
    let status = (student.status===1)?"students-table-status-green":"students-table-status-grey";

    var newRow = document.createElement("tr");
    newRow.id = student.studentId;
    newRow.innerHTML = `
    <td><input type="checkbox" class="students-table-checkbox"></td>
    <td>${group}</td>
    <td>${firstName} ${lastName}</td>
    <td>${gender}</td>
    <td>${birthday}</td>
    <td><div class=`+status+`></div></td>
    <td>
        <div class="students-table-options-cell-container">
            <div class="students-table-options-edit">
                <button class="btn btn-outline-success students-table-options-edit-button">
                    <img src="./res/pencil.png" alt="Table edit button">
                </button>
            </div>
            <div class="students-table-options-delete">
                <button class="btn btn-outline-danger students-table-options-delete-button">
                    <img src="./res/close.png" alt="Table delete button">
                </button>
            </div>
        </div>
    </td>
    `;

    tableBody.appendChild(newRow);
}

function emptyTable(){
    students = [];
    let rows = document.querySelectorAll("#students-table tbody tr");

    rows.forEach(r=>{
        r.remove();
    })
}