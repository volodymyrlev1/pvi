function sendDataToServer(studentG) {
    let group = $("#group").val();
    let firstName = $("#first-name").val();
    let lastName = $("#last-name").val();
    let gender = $("#gender").val();
    let birthday = $("#birthday").val();
    
    let student = new Student(group,firstName,lastName,gender,birthday,0);
    if(currentContext === "edit") student.studentId = studentG.studentId;
    
    let dataToSend = JSON.stringify(student);
    dataToSend = dataToSend.slice(0, -1) + ',"currentContext":"' + currentContext + '"}';

    console.log(dataToSend);
    $.ajax({
        type: "POST",
        url: "validation.php",
        data: dataToSend,
        success: function(response) {
            var responseData = JSON.parse(response);
            
            if(responseData.error) {
                console.error("Server Error:", responseData.error);
                showAlert(responseData.error);
            } else {
                console.log(`Data sent successfully. Response content: ${response}`);

                if(currentContext === "add") {
                    addStudent(student.studentId);
                }
                else if(currentContext === "edit"){
                    editStudent(row, studentG);
                }
            }
        },
        error: function(xhr, status, error) {
            console.error("Failed. Error:", error);
        }
    });

    fetchData(false);
    StudentButtonsInit();
}

function deleteStudent(sid) {
    let dataToSend = JSON.stringify(id = {sid});
    dataToSend = dataToSend.slice(0, -1) + ',"currentContext":"' + currentContext + '"}';
    console.log(dataToSend);

    $.ajax({
        type: "POST",
        url: "validation.php",
        data: dataToSend,
        success: function(response) {
            let responseData = JSON.parse(response);

            if(responseData.error){
                console.error("Server Error:", responseData.error);
                showAlert(responseData.error);
            } else {
                console.log("Success: ", responseData)
            }

        },
        error: function(xhr, status, error) {
            console.error("Failed. Error:", error);
        }
    });
}

function fetchData(addingEnabled = true) {
    students = []

    $.ajax({
        type: "GET",
        url: "read-data.php",
        success: function(response) {
            console.log(response);

            response.forEach( std => {
                const {student_group, first_name, last_name, gender, birthday, status, id} = std;
                
                let student = new Student(student_group, first_name, last_name, gender, birthday, status);
                student.studentId = id;
        
                if(addingEnabled)
                    addStudentsToTheTable(student);
                students.push(student);
            });
            if(addingEnabled) StudentButtonsInit();
        },
        error: function(xhr, status, error) {
            console.error("Failed. Error:", error);
        }
    });

    console.log(students);
}