<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lab4</title>
    <link rel="icon" href="">
    <link rel="stylesheet" href="./css/style.css">
    <!--font-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,opsz,wght@0,6..12,200..1000;1,6..12,200..1000&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css">

    <link rel="manifest" href="/manifest.json">
    <!-- jQuery library -->
    <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

    <script defer src="script.js"></script>
    <script defer src="tableScripts.js"></script>
    <script defer src="pagination.js"></script>
    <script defer src="server.js"></script>

    <script>
        const register = async () => {
            if ('serviceWorker' in navigator) {
            window.addEventListener('load', function() {
                navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
                    console.log('ServiceWorker registration successful with scope: ', registration.scope);
                }, function(err) {
                    console.log('ServiceWorker registration failed: ', err);
                });
            });
        }
        }

        register();
    </script>
</head>
<body>
    <?php require "header.php";?>
    <section id="page-working-section">
        <nav id="page-navigation-menu">
            <ul>
                <li><a href="#" id="page-navigation-menu-dashboard" onclick="showContent('dashboard')">Dashboard</a></li>
                <li><a href="#" id="page-navigation-menu-students" onclick="showContent('students')">Students</a></li>
                <li><a href="#" id="page-navigation-menu-tasks" onclick="showContent('tasks')">Tasks</a></li>
            </ul>
        </nav>

        <div id="page-working-section-right">
            <div id="dashboard-content" style="display: none;">
                <h2>Dashboard</h2>
            </div>
        
            <div id="students-content" style="display: none;">
                <h2>Students</h2>
                <div id="students-table-container">
                    <button type="button" class="btn btn-secondary" id="students-content-button-add">+</button>
                    <table id="students-table" class="table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th scope="col">
                                    <input type="checkbox" id="students-table-header-checkbox">
                                </th>
                                <th>Group</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Birthday</th>
                                <th>Status</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                    <div id="students-table-pagination">
                        <a href="#" class="pagination-button" data-page="-1">&lt;</a>
                        <a href="#" class="pagination-button active" data-page="1">1</a>
                        <a href="#" class="pagination-button" data-page="2">2</a>
                        <a href="#" class="pagination-button" data-page="3">3</a>
                        <a href="#" class="pagination-button" data-page="4">4</a>
                        <a href="#" class="pagination-button" data-page="+1">&gt;</a>
                    </div>
                      

                </div>
            </div>
        
            <div id="tasks-content" style="display: none;">
                <h2>Tasks</h2>
            </div>

            <!--Messages-->
            <div id="messages-content">
                <h2>Messages</h2>
            </div>
        </div>

        <div class="main-container">
            <div class="sub-container">
                <div class="sky">
                <div class="stars"></div>
                <div class="stars2"></div>
                <div class="stars3"></div>
                <div class="comet"></div>
                </div>
            </div>
        </div>
    </section>

    <!-- Inbox dropdown -->
    <div id="inbox-dropdown" style="display: none;">
        <div class="chat-user-message">
            <div class="chat-user">
                <img src="./res/user.png" alt="New message (User's profile picture)">
                <span class="chat-user-name">Admin</span>
            </div>
            <div class="chat-user-message-text-container"></div>
        </div>
        <div class="chat-user-message">
            <div class="chat-user">
                <img src="./res/user.png" alt="New message (User's profile picture)">
                <span class="chat-user-name">John</span>
            </div>
            <div class="chat-user-message-text-container"></div>
        </div>
        <div class="chat-user-message">
            <div class="chat-user">
                <img src="./res/user.png" alt="New message (User's profile picture)">
                <span class="chat-user-name">Tom</span>
            </div>
            <div class="chat-user-message-text-container"></div>
        </div>
    </div>

    <!-- Profile dropdown -->
    <div id="profile-dropdown" style="display: none;">
        <ul class="list-group">
            <li class="list-group-item">Profile</li>
            <li class="list-group-item">Logout</li>
          </ul>
    </div>

    <!--Confirmation window-->
    <div class="confirmation-container" id="confirmation">
        <div class="confirmation-header">
            <p style="font-family: 'Nunito Sans', sans-serif; font-weight: bold;">Warning</p>
            <button type="button" class="btn btn-outline-danger" onclick="hideConfirmation()" id="confirmation-button-close">
                <img src="./res/close.png" alt="Close button">
            </button>
        </div>

        <span class="confirmation-body" style="font-family: 'Nunito Sans', sans-serif; font-weight: bold;">
            Are you sure you want to delete?
        </span>

        <div class="confirmation-footer">
            <button type="button" class="btn btn-danger" onclick="deleteRow()" id="confirmation-button-ok">Ok</button>
            <button type="button" class="btn btn-dark" onclick="hideConfirmation()" id="confirmation-button-cancel">Cancel</button>
        </div>
    </div>

    <!--Add student form-->
    <div class="add-student-container" id="add-student-form" style="display: none;">
        <form id="add-student-form-container">
            <div class="add-student-form-contents">
                <div class="add-student-container-header">
                    <h2 style="font-family: 'Nunito Sans', sans-serif; font-weight: bold;">Add Student</h2>
                    <button type="button" class="btn btn-outline-danger" onclick="hideAddStudentForm()">
                        <img src="./res/close.png" alt="Close button">
                    </button>
                </div>
                <div class="add-student-container-body">
                    <fieldset>
                        <div class="add-student-fieldset">
                            <label for="group">Group: 
                                <select id="group" name="group" required="required">
                                    <option value="PZ-21">PZ-21</option>
                                    <option value="PZ-22">PZ-22</option>
                                    <option value="PZ-23">PZ-23</option>
                                    <option value="PZ-24">PZ-24</option>
                                </select>
                            </label>
                            <label for="first-name">First Name: <input type="text" id="first-name" name="first-name" required="required"></label>
                            <label for="last-name">Last Name: <input type="text" id="last-name" name="last-name" required="required"></label>
                            <label for="gender">Gender:
                                <select id="gender" name="gender" required="required">
                                    <option value="M">Male</option>
                                    <option value="F">Female</option>
                                    <option value="NB">Non-Binary</option>
                                </select>
                            </label>
                            <label for="birthday">Birthday: <input type="date" id="birthday" name="birthday" required="required"></label>
                        </div>
                    </fieldset>
                    <div class="container">
                        <div class="row justify-content-end">
                            <div class="col-auto">
                                <div class="alert alert-danger d-flex align-items-center" id ="alert-danger" role="alert">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                                    <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                    </svg>
                                    <span class="alert-text">
                                        Default Alert
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="add-student-container-footer">
                    <button type="button" class="btn btn-primary" onclick="hideAddStudentForm()">Ok</button>
                    <button type="button" class="btn btn-success" id='buttonAddEdit'>Create</button>
                </div>
            </div>
        </form>
    </div>
    
    <div id="overlay" class="overlay"></div>
</body>
</html>