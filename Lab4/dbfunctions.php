<?php
    function addStudentToDB($conn, $group, $first_name, $last_name, $gender, $birthday, $status, $student_id) {
        $sql = "INSERT INTO `students` (`student_group`, `first_name`, `last_name`, `gender`, `birthday`, `status`, `id`) 
                VALUES ('$group', '$first_name', '$last_name', '$gender', '$birthday', '$status', '$student_id')";

        try {
            $conn->exec($sql);
            return array('success' => "Student $student_id added successfully");
        } catch (PDOException $e) {
            return array('error' => 'Error inserting data: ' . $e->getMessage());
        }
    }

    function editStudentInDB($conn, $student_id, $group, $first_name, $last_name, $gender, $birthday, $status) {
        $sql = "UPDATE `students` 
                SET `student_group` = '$group', 
                    `first_name` = '$first_name', 
                    `last_name` = '$last_name', 
                    `gender` = '$gender', 
                    `birthday` = '$birthday', 
                    `status` = '$status' 
                WHERE `id` = '$student_id'";
    
        try {
            $affected_rows = $conn->exec($sql);
            if ($affected_rows > 0) {
                return array('success' => "Student $student_id updated successfully");
            } else {
                return array('error' => "No student found with ID $student_id");
            }
        } catch (PDOException $e) {
            return array('error' => 'Error updating data: ' . $e->getMessage());
        }
    }

    function deleteStudentFromDB($conn, $student_id) {
        $sql = "DELETE FROM students WHERE id = '$student_id'";

        try{
            $conn->exec($sql);
            return array('success' => "Student $student_id deleted successfully");
        } catch (PDOException $e) {
            return array('error' => 'Error deleting student: '.$e->getMessage());
        }
    }
?>