const socket = io('http://localhost:4000');

const messageForm = document.getElementById('chat-form');
const messageInput = document.getElementById('message-input');
const chatContainer = document.getElementById('chat-container');
const chatRoomChats = document.getElementById('chat-room-chats');

// socket.on('chat-message', (msg) => {
//     createMessage(msg, true);
//     console.log("RECIEVED");
// });

socket.on("recieve-messege", (username, message, timestamp) => {
    console.log("RECIEVED: ", username,message,timestamp);

    createMessage(username,message);
});

let userRooms;
let selectedRoomId;

socket.on('get-user-rooms-answer', (roomIds)=>{
    console.log(roomIds);

    while (chatRoomChats.firstChild) {
        chatRoomChats.removeChild(chatRoomChats.firstChild);
    }

    roomIds.forEach(room => {
        const div = document.createElement('div');
        div.id = room._id;
        div.textContent = room.name;
        div.classList.add('chat-room');
        div.addEventListener('click', (e) => {
            socket.emit("join-room", div.id);
            console.log("NOW IN ROOM ", room.name);
            selectedRoomId = room._id;
            showChatRoom(room._id);
        });
        chatRoomChats.appendChild(div);
    });
})

function showChatRoom(roomId){
    const chatFormWrapper = $('.chat-form-wrapper')[0];

    chatFormWrapper.style.display = "block";
}

messageForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const userName = $("#username-loginned").text();
    const userId = $("#id-loginned").text();
    const message = messageInput.value;
    
    socket.emit("chat messege",selectedRoomId,userName,userId,message);
    createMessage(userName, message, true);

    messageInput.value = '';
    console.log("SENT");
});

function createMessage(userName, text, ownMessage = false){
    const messageElement = document.createElement('div');
    const nameElement = document.createElement('span');
    messageElement.classList.add("message-recieved");
    if(ownMessage){
        messageElement.classList = [];
        messageElement.classList.add("message-sent");
    }

    messageElement.textContent = userName + ": " + text;
    chatContainer.appendChild(messageElement);
    messageElement.appendChild(nameElement);
}