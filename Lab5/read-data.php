<?php

    try{
        $conn = new PDO("mysql:host=localhost;dbname=studentsdb", "root", "");
    } 
    catch(PDOException $e){
        echo "Connection failed: " . $e->getMessage();
        exit;
    }

    $stmt = $conn->query("SELECT * FROM students");
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    header('Content-Type: application/json');
    echo json_encode($results);

?>