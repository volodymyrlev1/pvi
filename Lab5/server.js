const express = require('express');
const socketIO = require('socket.io');
const path = require('path');
const http = require('http');
const cors = require('cors');
const Room = require("./room");
const { ObjectId } = require('mongodb');

const app = express();
const server = http.createServer(app);

const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectId;
const uri = 'mongodb://localhost:27017';
const client = new MongoClient(uri);

client.connect((err) => {
    if (err) {
      console.error('Error connecting to MongoDB:', err);
      return;
    }
    console.log('Connected to MongoDB');
    const db = client.db('CMS'); 
  });

const io = socketIO(server, {
    cors: {
      origin: "http://localhost", 
      methods: ["GET", "POST"]
    }
});




app.use(cors());

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.php'));
});


app.get('/socket.io/socket.io.js', (req, res) => {
    console.log('Serving socket.io client library');
    const filePath = path.join(__dirname, 'node_modules', 'socket.io', 'client-dist', 'socket.io.js');
    console.log('File path:', filePath);
    res.sendFile(filePath);
});

const room = new Room();

// Handle Socket.IO connections
io.on('connection', async (socket) => {
    console.log('A user connected');

    let currentRoomId;

    socket.on("join-room",async (id)=>{
        
        currentRoomId = await room.joinRoom(id);
        socket.join(currentRoomId);

        console.log("user joined the room ", currentRoomId)
    })

    socket.on('chat messege', (selectedRoomId, username, userId, message) => {
        console.log(selectedRoomId, username,userId,message);

        // pushing messege to db
        const db = client.db('CMS');
        const chatRoomsCollection = db.collection('chatrooms');
        

        console.log(currentRoomId);

        const now = new Date();
        let timestamp = `${now.getHours()}:${String(now.getMinutes()).padStart(2, '0')}`;

        socket.to(currentRoomId).emit("recieve-messege",username, message, timestamp);
        //socket.to(roomID).emit("recieve-message", msg);        
    });

    socket.on('disconnect', () => {
          
        room.leaveRoom(currentRoomId);
        console.log('User disconnected');
    });

    socket.on('get-user-rooms', (userId) => {
        const db = client.db('CMS');
        const usersCollection = db.collection('UserData');
        const chatRoomsCollection = db.collection('chatrooms');

        console.log("GOT USER ID ", userId);
    
        usersCollection.findOne({ _id: new ObjectId(userId) })
            .then(user => {
                if (user) {
                    const roomIds = user.rooms;
                    return chatRoomsCollection.find({ _id: { $in: roomIds } }).toArray();
                } else {
                    throw new Error('User not found');
                }
            })
            .then(rooms => {
                socket.emit('get-user-rooms-answer', rooms);
            })
            .catch(error => {
                console.error('Error:', error);
            });
    });

    socket.on('create-chatroom', async (data) => {
        try {
            const { roomName, selectedUserIds } = data;
            const db = client.db('CMS');
            const chatRoomsCollection = db.collection('chatrooms');
            const usersCollection = db.collection('UserData');
        
            const newChatRoom = {
                name: roomName,
                messages: []
            };
        
            const result = await chatRoomsCollection.insertOne(newChatRoom);

            // push new room id to users
            for(let i = 0; i < selectedUserIds.length; i++)
            {
                const res = await usersCollection.updateOne(
                    { _id: new ObjectID(selectedUserIds[i]) },
                    { $push: { rooms: result.insertedId } }
                );
            }

            console.log(`New chat room created with id ${result.insertedId}`);

            //socket.emit('chatroom-created', chatroomIds);
        } catch (err) {
            console.error('Error creating chat room:', err);
            socket.emit('chatroom-created', { error: 'Internal Server Error' });
        }
    });
});

const PORT = process.env.PORT || 4000;
server.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});