<?php
$conn;
try {
    $conn = new MongoDB\Driver\Manager("mongodb://localhost:27017");
} catch (MongoDB\Driver\Exception\Exception $e) {
    echo 'Failed to connect to MongoDB, is the service installed and running?<br /><br />';
    echo $e->getMessage();
    exit();
}

$dbName = 'CMS';

$query = new MongoDB\Driver\Query([]);

$cursor = $conn->executeQuery("$dbName.UserData", $query);

$users = array();
foreach ($cursor as $doc) {
    $users[] = $doc;
}

echo json_encode($users);
?>
