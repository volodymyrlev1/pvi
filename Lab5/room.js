
const { v4: uuidv4 } = require('uuid');

class Room{
    constructor(){
        this.roomsState = []
    }

    joinRoom(newID) {
        return new Promise((resolve) => {
            this.roomsState.push({
                id: newID,
                users: 1,
            });
            return resolve(newID);
        });
    }

    leaveRoom(id) {
        this.roomsState = this.roomsState.filter((room) => {
        if (room.id === id) {
            if (room.users === 1) {
            return false;
            } else {
            room.users--;
            }
        }
        return true;
        });
    }
}

module.exports = Room;