const chatRooms = [];

function createChatRoom(roomName){
    const newChatRoom = document.createElement('div');

    newChatRoom.dataset.roomName = roomName;
    newChatRoom.innerHTML = `
        <div class="chat-room">
            <h5>${roomName}</h5>
            <button class="add-user-btn">+ Add user</button>
        </div>
    `;

    document.querySelector('#page-working-section #chat-room-chats').appendChild(newChatRoom);

    chatRooms.push({
        name: roomName,
        element: newChatRoom
    });

    newChatRoom.addEventListener('click', () => {
        const roomName = newChatRoom.dataset.roomName;
        showChatRoom(roomName);
    });
}


// creation window 
$('#page-working-section #messages-content .chat-room-header button').click(() => {
    let container = $('.chat-room-creation-container')[0];
    if (container.style.display === 'block') {
        container.style.display = 'none';
        document.getElementById('overlay').style.display = 'none';
    } else {
        container.style.display = 'block';
        document.getElementById('overlay').style.display = 'block';
    }
});

$("#chat-room-creation-button-cancel").click(()=>{
    let container = $('.chat-room-creation-container')[0];
    container.style.display = 'none';
    document.getElementById('overlay').style.display = 'none';
})

$("#chat-room-creation-button-ok").click(async () => {
    const roomName = $("#chat-room-creation-form-input-chat-name").val();
    const selectedUserIds = $("#user-list input:checked").map((i, checkbox) => $(checkbox).attr("id")).get();


    if (roomName && selectedUserIds.length > 0) {
    try {
        console.log(selectedUserIds);
        socket.emit("create-chatroom",{roomName,selectedUserIds});
    } catch (err) {
        console.error('Error creating chat room:', err);
    }
    } else {
        console.error("Please enter a room name and select at least one user.");
    }

    let container = $('.chat-room-creation-container')[0];
    container.style.display = 'none';
    document.getElementById('overlay').style.display = 'none';
});

function fetchUsers() {
    $.ajax({
        url: 'fetch-users-mongo.php',
        type: 'GET',
        success: function(response) {
            populateUserList(JSON.parse(response));
        },
        error: function(xhr, status, error) {
            console.error('Error fetching users:', error);
        }
    });
}

let sessionUserId;
$("#page-header-right-notifications-content").click(()=>{
    fetchUsers();
})


function populateUserList(users) {
    var userListContainer = $('#user-list');
    sessionUserId = $("#id-loginned").text();
    
    socket.emit('get-user-rooms',sessionUserId);

    users.forEach(function(user) {
        
        var checkboxDiv = $('<div>').addClass('form-check');
        
        var checkbox = $('<input>').addClass('form-check-input').attr({
            type: 'checkbox',
            value: user.username,
            id: user._id.$oid
        });
        
        console.log($("#id-loginned").text())

        if(user._id.$oid === sessionUserId){
            checkbox.prop('checked',true).on('click', function(e) {
                e.preventDefault(); 
            });
        }

        var label = $('<label>').addClass('form-check-label').attr('for', checkbox.attr('id')).text(user.username);

        checkboxDiv.append(checkbox).append(label);
        userListContainer.append(checkboxDiv);
    });
}

var userList = document.getElementById('user-list');
userList.style.overflowY = 'scroll';
userList.style.maxHeight = '200px'; 