<header id="page-header">
    <div id="page-header-container">
        <span id="page-header-left-span">CMS</span>
        <div id="page-header-right-notifications">
            <div id="page-header-right-notifications-content">
                <img src="./res/notification.png" alt="" id="notification-icon">
                <div id ="circle-id" class="Circle" ></div>
            </div>

        </div>
        <div id="page-header-right-user-summary">
            <div id="page-header-profile-picture">
                <img src="./res/user.png" alt="Profile Picture">
            </div>
            
            <?php
                session_start();

                if (isset($_SESSION['username'])) {
                    $username = $_SESSION['username'];
                    $id = $_SESSION['id'];
                    echo "Welcome, <span id='username-loginned'>$username</span> (<span id='id-loginned'>$id</span>)!";
                } else {
                    echo '<a href="login.html" id="login-href">
                            <button id="login-button">Log in</button>
                        </a>';
                }
            ?>
        </div>
    </div>
</header>