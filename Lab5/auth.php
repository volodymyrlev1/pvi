<?php
    session_start();

    $conn;
    try {
        $conn = new MongoDB\Driver\Manager("mongodb://localhost:27017");
    } catch (MongoDBDriverExceptionException $e) {
        echo 'Failed to connect to MongoDB, is the service installed and running?<br /><br />';
        echo $e->getMessage();
        exit();
    }

    $username = $_POST['username'];
    $password = $_POST['password'];

    $query = new MongoDB\Driver\Query(['username' => $username]);

    $result = $conn->executeQuery('CMS.UserData', $query);

    $user = $result->toArray()[0] ?? null;

    if ($user) {
        if ($user->password == $password) {
            $_SESSION['username'] = $username;
            $_SESSION['id'] = $user->_id;
            header("Location: index.php"); 
            exit();
        } else {
            echo "Incorrect password!";
        }
    } else {
        echo "User not found!";
    }

?>
