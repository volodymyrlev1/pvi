let students = [
    new Student("PZ-22", "Volodymyr", "Lev", "M", "2005-07-30"),
    new Student("PZ-22", "Artur", "Husar", "M", "2005-01-11"),
    new Student("PZ-22", "Oleh", "Shmyheliuk", "M", "2005-05-29")
];

// add update table function to become DRY
function addStudent(){
    var group = document.getElementById("group").value;
    var firstName = document.getElementById("first-name").value;
    var lastName = document.getElementById("last-name").value;
    var gender = document.getElementById("gender").value;
    var birthday = document.getElementById("birthday").value;

    let std = new Student(group, firstName, lastName, gender,birthday);
    students.push(std);

    var tableBody = document.querySelector("#students-table tbody");
    var newRow = document.createElement("tr");

    console.log(group,firstName,lastName,gender,birthday);

    newRow.innerHTML = `
        <td><input type="checkbox" class="students-table-checkbox"></td>
        <td>${group}</td>
        <td>${firstName} ${lastName}</td>
        <td>${gender}</td>
        <td>${birthday}</td>
        <td><div class="students-table-status-grey"></div></td>
        <td>
            <div class="students-table-options-cell-container">
                <div class="students-table-options-edit">
                    <button class="students-table-options-edit-button">
                        <img src="./res/pencil.png" alt="Table edit button">
                    </button>
                </div>
                <div class="students-table-options-delete">
                    <button class="students-table-options-delete-button">
                        <img src="./res/close.png" alt="Table delete button">
                    </button>
                </div>
            </div>
        </td>
    `;

    tableBody.appendChild(newRow);

    document.getElementById("group").value = "";
    document.getElementById("first-name").value = "";
    document.getElementById("last-name").value = "";
    document.getElementById("gender").value = "";
    document.getElementById("birthday").value = "";
    hideAddStudentForm();

    StudentButtonsInit();
}

let rowToDelete;
function StudentButtonsInit() {
    let confirmationWindow = document.getElementById("confirmation");
    let deleteButtons = document.getElementsByClassName("students-table-options-delete-button");

    for(let i = 0; i < deleteButtons.length; i++)
    {
        deleteButtons[i].addEventListener("click", function(){
            
            rowToDelete = this.closest("tr");
            let studentName = rowToDelete.querySelector("td:nth-child(3)").textContent;
            showConfirmation(studentName);

            document.getElementById("confirmation-button-ok").addEventListener("click", function() {
                deleteRow(rowToDelete);
                hideConfirmation();
            });

            // Cancel button
            document.getElementById("confirmation-button-cancel").addEventListener("click", function() {
                hideConfirmation();
            });
        });
    }

    let headerCheckbox = document.getElementById('students-table-header-checkbox');
    let rowCheckboxes = document.querySelectorAll('.students-table-checkbox');

    headerCheckbox.addEventListener('click', function() {
        rowCheckboxes.forEach(checkbox => {
          checkbox.checked = headerCheckbox.checked;
        });
      });
      
      rowCheckboxes.forEach(checkbox => {
          checkbox.addEventListener('click', () => {
            const allChecked = Array.from(rowCheckboxes).every(checkbox => checkbox.checked);
            headerCheckbox.checked = allChecked;
          });
      });
}

// ADD WHEN USER DELETES STUDENT FROM TABLE HE IS ALSO DELETES FROM student LIST
function deleteRow(row) {
    row.remove();
    hideConfirmation();
}