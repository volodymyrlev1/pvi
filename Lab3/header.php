<header id="page-header">
    <div id="page-header-container">
        <span id="page-header-left-span">CMS</span>

        <div id="page-header-right-notifications">
            <div id="page-header-right-notifications-content">
                <img src="./res/notification.png" alt="" id="notification-icon">
                <div id ="circle-id" class="Circle" ></div>
            </div>

        </div>
        <div id="page-header-right-user-summary">
            <div id="page-header-profile-picture">
                <img src="./res/user.png" alt="Profile Picture">
            </div>
            <span id="page-header-profile-name">Volodymyr Lev</span>
        </div>
    </div>
</header>