function sendDataToServer(studentG) {
    let group = $("#group").val();
    let firstName = $("#first-name").val();
    let lastName = $("#last-name").val();
    let gender = $("#gender").val();
    let birthday = $("#birthday").val();

    let student = new Student(group,firstName,lastName,gender,birthday);

    let dataToSend = JSON.stringify(student);
    console.log(dataToSend);
    $.ajax({
        type: "POST",
        url: "validation.php",
        data: dataToSend,
        success: function(response) {
            var responseData = JSON.parse(response);
            
            if(responseData.error) {
                console.error("Server Error:", responseData.error);
                showAlert(responseData.error);
            } else {
                console.log(`Data sent successfully. Response content: ${response}`);

                if(currentContext === "add") {
                    addStudent();
                } 
                else if(currentContext === "edit")
                {
                    editStudent(row, studentG);
                }
            }
        },
        error: function(xhr, status, error) {
            console.error("Failed. Error:", error);
        }
    });
}
