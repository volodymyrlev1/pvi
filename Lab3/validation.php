<?php
    $jsonData = file_get_contents("php://input");
    $decodedData = json_decode($jsonData, true);

    if($decodedData !== null){
        $error = "";

        if(isset($decodedData["group"]) && $decodedData["group"] !== null){
            $group = $decodedData["group"]; 
        } else {
            echo json_encode(array('error' => 'Error: Group is required.'));
            exit;
        }

        if(isset($decodedData["firstName"]) && isset($decodedData["lastName"])){
            $first_name = $decodedData["firstName"];
            $last_name = $decodedData["lastName"];
            if(empty($first_name) || empty($last_name) || !ctype_alpha($first_name) || !ctype_alpha($last_name)){
                echo json_encode(array('error' => 'Error: First name and last name should only contain alphabetical characters and cannot be empty.'));
                exit;
            }
        } else {
            echo json_encode(array('error' => 'Error: First name and last name are required.'));
            exit;
        }

        if(isset($decodedData["gender"]) && $decodedData["gender"] !== null){
            $gender = $decodedData["gender"];
        } else {
            echo json_encode(array('error' => 'Error: Gender is required.'));
            exit;
        }

        if(isset($decodedData["birthday"]) && !empty($decodedData["birthday"])){
            $birthday = $decodedData["birthday"];
            $currentDate = new DateTime();
            $providedDate = new DateTime($birthday);
            $age = $currentDate->diff($providedDate)->y;
            if($age < 16){
                echo json_encode(array('error' => 'Error: The person must be 16 years or older.'));
                exit;
            }
        } else {
            echo json_encode(array('error' => 'Error: Birthday is required.'));
            exit;
        }

        echo json_encode(array('success' => "Success: data validated successfully"));
    } else {
        echo json_encode(array('error' => 'Error: JSON data not received.'));
    }
?>
